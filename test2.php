<?php 
public function post_confirm()
{
	$id = Input::get('service_id');
	$servicio = Service::find($id);
  	if($servicio == NULL) // Se reordenaron las condiciones para reducir l�neas
	{
		return Response::json(array('error' => '3'));
    }
	if($servicio->status_id == '6')
	{
		return Response::json(array('error' => '2'));
	}
	if(($servicio->driver_id != NULL) || ($servicio -> status_id != '1'))
    {
    	return Response::json(array('error' => '1'));  
    }
	
  	$driver = Input::get('driver_id'); //la variable $driver se declara desde un principio para evitar llamar repetitivamente Input::get('driver_id');
	$servicio = Service::update($id, array('driver_id' => $driver, 'status_id' => '2'));
	Driver::update($driver, array("available" => '0'));
    // Como se llama una sola vez esa funci�n se omite $driverTmp = Driver::find( $driver);
	Service::update($id, array('car_id' => Driver::find( $driver)->car_id));
	$pushMessage = 'Tu servicio ha sido confirmado';
	$servicio = Service::find($id);
	$push = Push::make();
	// para no repetir return Response::json(array('error' => "0")), se hace la condici�n $servicio->user->uuid != "" para evitar que se ejecute el env�o de resultados si $servicio->user->uuid es vac�o 
  	if($servicio->user->uuid != "")
	{
		if($servicio->user->type == '1')
		{
			$result = $push->ios($servicio->user->uuid, $pushMessage, 1, 'honk.wav' , 'Open' ,  array('serviceId' => $servicio->$id));
		}
		else
		{
			$result = $push->android2($servicio->user->uuid, $pushMessage, 1, 'default' , 'Open' ,  array('serviceId' => $servicio->$id));	
		}
	}
  	return Response::json(array('error' => "0"));
}
?>