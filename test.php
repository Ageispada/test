<?php
	set_error_handler('exceptions_error_handler');
	
	function exceptions_error_handler($severity, $message, $filename, $lineno) {
	  if (error_reporting() == 0) {
		return;
	  }
	  if (error_reporting() & $severity) {
		throw new ErrorException($message, 0, $severity, $filename, $lineno);
	  }
	}

	set_error_handler('exceptions_error_handler');
	$v = new vista();
	$_fp = $v->get("stdin");
	try 
	{
		if ($_fp == "")
		{
			echo $v->escribir_formulario();
		}
		else
		{
			$fp  = explode(PHP_EOL, $_fp);
			$c = new controlador();
			$t = $fp[0];
			$x = 0;
			for($i = 0; $i < $t ; $i++)
			{
				$x++;
				$m = $c->leer_n_m($fp[$x]);
				for($j = 0; $j < $m ; $j++)
				{
					$x++;
					echo $c->operar($fp[$x]);	
				}
			}
		}
	} 
	catch (Exception $e) 
	{
    	echo "Error general";
	}

	class modulo
	{
		var $cubo=array();
		var $tamano = 0;
		function modulo($n)
		{
			for($x = 1; $x <= $n; $x++)
			{
				for($y = 1; $y <= $n; $y++)	
				{
					for($z = 1; $z <= $n; $z++)
					{
						$this->cubo[$x][$y][$z] = 0;
					}
				}
			}
			$this->tamano = $n;
		}
		
		function consultar($x, $y, $z)
		{
			return $this->cubo[$x][$y][$z];
		}
		
		function actualizar($x, $y, $z, $w)
		{
			$this->cubo[$x][$y][$z] = $w ;
		}
	}

	class controlador
	{
		var $m;
		function leer_t($fp)
		{
			return fgets($fp);
		}
		function leer_n_m($fp)
		{
			$linea = explode( ' ', $fp);
			$this->m = new modulo($linea[0]);
			return $linea[1];
		}
	
		function operar($fp)
		{
			$linea = explode( ' ', $fp );
			$k = "";
			switch ($linea[0]) 
			{
				case "UPDATE":
					$this->update($linea[1] , $linea[2] , $linea[3] , $linea[4]);
				break;	
				case "QUERY":
					$k = $this->query($linea[1] , $linea[2], $linea[3], $linea[4], $linea[5] , $linea[6])."<br/>";
				break;	
				default:
					$k = "error de operaci&oacute;n<br/>";
			}
			return $k;
		}
		
		function update($x, $y, $z, $w)
		{
			$this->m->actualizar($x,$y,$z,$w);	
		}
		
		function query($x1, $y1, $z1, $x2, $y2 , $z2)
		{
			$w = 0;
			$x = 0;
			$y = 0;
			$z = 0;
			for($x = $x1; $x <= $x2; $x++)
			{
				for($y = $y1; $y <= $y2; $y++)
				{
					for($z = $z1; $z <= $z2; $z++)
					{
						$w +=$this->m->consultar($x , $y, $z);
					}				
				}			
			}
			return $w;
		}
	}

	class vista
	{
		public static function get($variable)
		{
			$entrada = "";
			if(isset($_GET[$variable]))
			{
				$entrada = $_GET[$variable];
			}
			return filter_var($entrada, FILTER_SANITIZE_STRING, FILTER_SANITIZE_SPECIAL_CHARS);
		}
		
		public static function escribir_formulario()
		{
			return "<body><form id=\"form1\" name=\"form1\" method=\"get\" action=\"test.php\" target=\"_self\">
					<p>
						<textarea name=\"stdin\" rows=\"10\" id=\"stdin\"></textarea>
					</p>
					<p>
						<input name=\"enviar\" type=\"submit\" value=\"enviar\" />
					</p>
				</form></body>";	
		}
	}
?>
